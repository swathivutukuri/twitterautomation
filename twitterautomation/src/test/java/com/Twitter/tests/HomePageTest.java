package com.Twitter.tests;

import com.Twitter.pages.HomePage;
import org.testng.annotations.Test;


public class HomePageTest extends TestBase {

    /**
     * @Method: clickBtnOnHomePage
     * @Description: This method will click a button on the home page.
     * @author :swathi Gunuputi
     */
    @Test
    public void clickBtnOnHomePage()  {
        HomePage homePage = new HomePage(driver);
        homePage.clickCreateAccountBtn();
    }


}





