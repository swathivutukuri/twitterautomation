package com.Twitter.tests;


import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;


public class TestBase {

    public static AppiumDriver driver;


    void initConfiguration() throws IOException {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        Properties androidConfig = new Properties();
        androidConfig.load(loader.getResourceAsStream("com/Twitter/config/androidConfig.properties"));
        File appdir = new File(System.getProperty("user.dir") + "/src/test/resources/com/Twitter/mobileApps/" + androidConfig.getProperty("androidApkName") + ".apk");

        //setting the desired capabilities of android driver
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("platformVersion", androidConfig.getProperty("androidDeviceVersion"));
        capabilities.setCapability("deviceName", androidConfig.getProperty("androidDeviceName"));
        capabilities.setCapability("app", appdir.getAbsolutePath());
        capabilities.setCapability("appActivity", androidConfig.getProperty("appActivity"));
        capabilities.setCapability("appPackage", androidConfig.getProperty("appPackage"));
        capabilities.setCapability("androidDeviceReadyTimeout", 60);
        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);

    }


    @BeforeMethod
    void beforeMethod() throws IOException {
        initConfiguration();

    }

    @AfterMethod
    void afterMethod() {
        driver.quit();
    }


}
