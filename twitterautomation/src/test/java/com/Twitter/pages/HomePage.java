package com.Twitter.pages;

import com.Twitter.fixtures.WaitTime;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.concurrent.TimeUnit;


public class HomePage {

    @AndroidFindBy(id = "com.twitter.android:id/primary_action")
    public MobileElement createAccountBtn;


    public AppiumDriver driver;

    public HomePage(AppiumDriver driver) {
        PageFactory.initElements(new AppiumFieldDecorator(driver, WaitTime.PAGE_FACTORY_WAIT_TIME, TimeUnit.SECONDS),
                this);
        this.driver = driver;

    }


    /**
     * @Method: clickCreateAccountBtn
     * @param:
     * @Description: This method will click on create account button.
     * @author :swathi Gunuputi
     */
    public void clickCreateAccountBtn() {
        new WebDriverWait(driver, WaitTime.WAIT).until(ExpectedConditions.visibilityOf(createAccountBtn));
        createAccountBtn.click();


    }
}
