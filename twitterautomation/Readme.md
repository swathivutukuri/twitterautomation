Twitter App Automation framework
============================
This is a reusable test automation framework for automating test scripts on Mobile Applications.

The framework is coupled with open source options such as Appium, Testng

A Page Object Model is followed for a better maintenance and reusability.

Prerequisite
------------
JDK 1.8

Maven 3.3.9

NPM 5.3.0

Appium latest version

Android Studio 2.3.3


Running your tests in local
---------------------------

Start appium using pre-launch as:

appium

Apk Download:
--------------
Download the apk file from the below link and rename the apk file as "Twitter.apk" and place it in src/test/resources/com/Twitter/mobileApps
https://apkpure.com/twitter/com.twitter.android/download?from=details

Note:
In the stash ,I am attaching a sample apk file under that path.

update the androidConfig.properties file under resources/com/Twitter/config package with device name and version.


Run the tests using :

mvn clean test


Project Download:
----------------------
Download the project using the below link
https://bitbucket.org/swathivutukuri/twitterautomation/src/master/twitterautomation/


Android Emulator
---------------------------
What you need:
- adb installed and ANDROID_HOME path set
- Device manager installed on Android studio

Setting up :
1. Open Android Studio -> Android Virtual Device Manager
2. Click on Create new Virtual Device.
3. Select the device specs you want to emulate. If it doesnt exist. Google is ur best friend and you can set it manually
4. Select the OS from recommended tab. (Download if need be)
5. Device should be set up. Click green triangle to run.

Confirm that adb can see the device on terminal/cmd with
"adb devices"
It will list connected device names E.g. emulator-5554

Android real device
---------------------------
What you need:
- real android device with developer options enable + usb debugging
- cable to connect device to mac/windows


Setting up:
1. Plug device in.
2. Allow permissions and usb debugging
3. Check adb devices pick up device

Author
--------
Swathi Gunuputi
